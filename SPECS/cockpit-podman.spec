# This spec file has been automatically updated
Version:        11
Release: 1%{?dist}
Name:           cockpit-podman
Summary:        Cockpit component for Podman containers
License:        LGPLv2+
URL:            https://github.com/cockpit-project/cockpit-podman

Source0:        https://github.com/cockpit-project/cockpit-podman/releases/download/%{version}/cockpit-podman-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  libappstream-glib

Requires:       cockpit-bridge >= 138
Requires:       cockpit-shell >= 138
Requires:       podman >= 1.3.0

%description
The Cockpit user interface for Podman containers.

%prep
%setup -q -n cockpit-podman

%build
# Nothing to build

%install
%make_install
appstream-util validate-relax --nonet %{buildroot}/%{_datadir}/metainfo/*

%files
%{_datadir}/cockpit/*
%{_datadir}/metainfo/*

%changelog
* Wed Dec 11 2019 Jindrich Novy <jnovy@redhat.com> - 11-1
- Fix Alert notification in Image Search Modal
- Allow more than a single Error Notification for Container action errors
- Various Alert cleanups
- Translation updates
- Related: RHELPLAN-25139

* Wed Nov 13 2019 Matej Marusak <mmarusak@redhat.com> - 10-1
- Support for user containers
- Show list of containers that use given image
- Show placeholder while loading containers and images
- Fix setting memory limit rhbz#1732713
- Add container Terminal rhbz#1703245

* Wed Jun 26 2019 Martin Pitt <mpitt@redhat.com> - 4-1
- Fix regression in container commit
- Fix AppStream ID rhbz#1734809

* Mon Jun 17 2019 Martin Pitt <mpitt@redhat.com> - 3-1
- Enable Commit button for running containers
- Fix race condition with container deletion
- Stop fetching all containers/images for each container/image event

* Sun Jun 09 2019 Martin Pitt <mpitt@redhat.com> - 2-2
- Fix podman dependency

* Mon May 27 2019 Martin Pitt <mpitt@redhat.com> - 2-1
- Update to upstream 2 release
- Support podman API 1.3
- Support running commands with arguments
- Show the default command coming from image
- Implement filtering of images and containers

* Wed Apr 17 2019 Cockpit Project <cockpituous@gmail.com> - 1-2
- Update to upstream 1 release

